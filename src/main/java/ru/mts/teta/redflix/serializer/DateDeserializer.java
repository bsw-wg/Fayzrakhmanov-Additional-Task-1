package ru.mts.teta.redflix.serializer;

import ru.mts.teta.redflix.advisor.exception.BadDateException;

import java.time.ZoneId;
import java.io.IOException;
import java.text.ParseException; 
import java.util.Locale;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import java.time.LocalDate;
import com.fasterxml.jackson.core.JsonParser; 
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize; 
import com.fasterxml.jackson.databind.deser.std.StdDeserializer; 


public class DateDeserializer extends StdDeserializer<LocalDateTime> {
   private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMMM yyyy года", new Locale("ru", "RU"));
   public DateDeserializer() { 
      this(null); 
   } 
   public DateDeserializer(Class<LocalDateTime> t) { 
      super(t); 
   } 

   @Override 
   public LocalDateTime deserialize(JsonParser parser, DeserializationContext context) throws IOException{
      String date = parser.getText();
      try {
         return LocalDate.parse(date, formatter).atTime(0, 0);
      } catch (Exception e) {
         throw new IOException();
      }
   }   
}