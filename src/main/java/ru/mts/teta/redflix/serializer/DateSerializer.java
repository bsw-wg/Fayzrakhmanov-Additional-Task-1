package ru.mts.teta.redflix.serializer;

import java.io.IOException;
import java.util.Locale;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider; 
import com.fasterxml.jackson.databind.annotation.JsonSerialize; 
import com.fasterxml.jackson.databind.ser.std.StdSerializer;


public class DateSerializer extends StdSerializer<LocalDateTime> {
   private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMMM yyyy года", new Locale("ru", "RU"));
   public DateSerializer() { 
      this(null); 
   } 
   public DateSerializer(Class<LocalDateTime> t) { 
      super(t); 
   } 
   @Override 
   public void serialize(LocalDateTime value, JsonGenerator generator, SerializerProvider arg2) throws IOException { 
      generator.writeString((String) formatter.format(value));
   } 
}
