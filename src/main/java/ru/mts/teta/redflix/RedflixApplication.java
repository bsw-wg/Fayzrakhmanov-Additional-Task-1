package ru.mts.teta.redflix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RedflixApplication {
	public static void main(String[] args) {
		SpringApplication.run(RedflixApplication.class, args);
	}

}
