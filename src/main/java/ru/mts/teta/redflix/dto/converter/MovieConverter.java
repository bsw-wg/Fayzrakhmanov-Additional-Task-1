package ru.mts.teta.redflix.dto.converter;

import ru.mts.teta.redflix.model.Movie;
import ru.mts.teta.redflix.dto.model.MovieDto;

import org.springframework.stereotype.Component;

import java.util.stream.Collectors;
import java.util.List;

@Component
public class MovieConverter {

	public MovieDto fromEntityToDto(Movie entity) {
		MovieDto dto = new MovieDto();
		dto.setId(entity.getId());
		dto.setReleaseDate(entity.getReleaseDate());
		dto.setGenre(entity.getGenre());
		dto.setTitle(entity.getTitle());
		return dto;
	}

	public Movie fromDtoToEntity(MovieDto dto) {
		Movie entity = new Movie();
		entity.setId(dto.getId());
		entity.setReleaseDate(dto.getReleaseDate());
		entity.setTitle(dto.getTitle());
		entity.setGenre(dto.getGenre());
		return entity;
	}

	public List<MovieDto> fromEntityToDtoList(List<Movie> entities) {
		return entities.stream().map(x -> this.fromEntityToDto(x)).collect(Collectors.toList());
	}

	public List<Movie> fromDtoToEntityList(List<MovieDto> dtos) {
		return dtos.stream().map(x -> this.fromDtoToEntity(x)).collect(Collectors.toList());
	}

}