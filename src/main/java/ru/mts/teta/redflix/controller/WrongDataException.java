package ru.mts.teta.redflix.controller;

public class WrongDataException extends Exception {
	private String message = "Неверные данные";

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}