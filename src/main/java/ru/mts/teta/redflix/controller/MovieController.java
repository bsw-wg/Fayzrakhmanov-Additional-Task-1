package ru.mts.teta.redflix.controller;

import ru.mts.teta.redflix.advisor.exception.BadDateException;
import ru.mts.teta.redflix.advisor.exception.NoSuchMovieException;
import ru.mts.teta.redflix.advisor.exception.WrongDataException;
import ru.mts.teta.redflix.model.Movie;
import ru.mts.teta.redflix.dto.model.MovieDto;
import ru.mts.teta.redflix.dto.converter.MovieConverter;
import ru.mts.teta.redflix.service.MovieService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/rest/movie")
public class MovieController {

    @Autowired
    private MovieService service;

    @Autowired
    private MovieConverter movieConverter;

    @GetMapping
    public List<MovieDto> findAll() {
        return movieConverter.fromEntityToDtoList(service.findAll());
    }

    @PostMapping
    public MovieDto addMovie(@RequestBody MovieDto dto) throws BadDateException, WrongDataException {
        if (dto.getReleaseDate() == null)
            throw new BadDateException("null");
        Movie movie = movieConverter.fromDtoToEntity(dto);
        Optional<Movie> result = service.addMovie(movie);
        if (!result.isPresent()) 
            throw new WrongDataException();
        return movieConverter.fromEntityToDto(result.get());
    }

    @GetMapping("/{id}")
    public MovieDto findMovieByid(@PathVariable("id") Long movieId) throws NoSuchMovieException {
        Optional<Movie> result = service.findMovieById(movieId);
        if (!result.isPresent()) {
            throw new NoSuchMovieException(movieId);
        }
        return movieConverter.fromEntityToDto(result.get());
    }

    @PutMapping("/{id}")
    public MovieDto updateMovieById(@RequestBody MovieDto dto, @PathVariable("id") Long movieId) throws WrongDataException, BadDateException {
        if (dto.getReleaseDate() == null)
        {
            throw new BadDateException("null");
        }
        Movie movie = movieConverter.fromDtoToEntity(dto);
        movie.setId(movieId);
        Optional<Movie> result = service.updateMovieById(movie);
        if (!result.isPresent()) {
            throw new WrongDataException();
        }
        return movieConverter.fromEntityToDto(result.get());
    }

    @DeleteMapping("/{id}")
    public String removeMovieById(@PathVariable("id") Long movieId) throws NoSuchMovieException {
        boolean result = service.removeMovieById(movieId);
        if (!result) {
            throw new NoSuchMovieException(movieId);
        }
        return "Успешно";
    }
}
