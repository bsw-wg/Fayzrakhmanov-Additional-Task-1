package ru.mts.teta.redflix.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import ru.mts.teta.redflix.dao.IMDBMovieRepository;
import ru.mts.teta.redflix.dao.InMemoryMovieRepository;
import ru.mts.teta.redflix.dao.MovieRepository;

@Configuration
public class MovieRepositoryConfig {

    @Bean
    @Profile("!prod")
    public MovieRepository inMemoryMovieRepository() {
        return new InMemoryMovieRepository();
    }

    @Bean
    @Profile("prod")
    public IMDBMovieRepository imdbMovieRepository() {
        return new IMDBMovieRepository();
    }
}
