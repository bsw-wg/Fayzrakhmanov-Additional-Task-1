package ru.mts.teta.redflix.dao;

import ru.mts.teta.redflix.model.Movie;

import java.util.Collections;
import java.util.List;
import java.util.Optional;


public class IMDBMovieRepository implements MovieRepository {

    @Override
    public List<Movie> findAll() {
        return Collections.emptyList();
    }

    @Override
    public Optional<Movie> addMovie(Movie movie) {
        return Optional.ofNullable(movie);
    }

    @Override
    public Optional<Movie> findMovieById(Long id) {
        return Optional.empty();
    }

    @Override
    public Optional<Movie> updateMovieById(Movie movie) {
        return Optional.empty();
    }

    @Override
    public boolean removeMovieById(Long id) {
        return false;
    }
}
