package ru.mts.teta.redflix.dao;

import ru.mts.teta.redflix.model.Movie;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.concurrent.ConcurrentHashMap;  
import java.util.List;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Map;


public class InMemoryMovieRepository implements MovieRepository {
    private final Map<Long, Movie> movieMap = new ConcurrentHashMap<>();
    private Long counter = new Long(0);

    @PostConstruct
    public void addSomeData() {
        this.addMovie(new Movie("Терминатор 2", LocalDateTime.of(2001, 1, 1, 0, 0), "Триллер"));
        this.addMovie(new Movie("Черное зеркало", LocalDateTime.of(2011, 12, 4, 0, 0), "Фантастика"));
        this.addMovie(new Movie("Кремниевая долина", LocalDateTime.of(2014, 5, 6, 0, 0), "Комедия"));
        this.addMovie(new Movie("Только что сняли", LocalDateTime.now(), "Драма"));
    }

    @Override
    public List<Movie> findAll() {
        return new ArrayList(movieMap.values());
    }

    @Override
    public Optional<Movie> addMovie(Movie movie) {
        if (movie.getReleaseDate() == null || movie.getGenre() == null || movie.getTitle() == null)
            return Optional.empty();
        movie.setId(counter);
        counter += 1;
        this.movieMap.put(movie.getId(), movie);
        return Optional.ofNullable(this.movieMap.get(movie.getId()));
    }

    @Override
    public Optional<Movie> findMovieById(Long id) {
        return Optional.ofNullable(this.movieMap.get(id));
    }

    @Override
    public Optional<Movie> updateMovieById(Movie movie) {
        if (movie.getReleaseDate() == null || movie.getGenre() == null || movie.getTitle() == null)
            return Optional.empty();
        if (!this.movieMap.containsKey(movie.getId())) {
            return Optional.empty();
        }
        this.movieMap.put(movie.getId(), movie);
        return Optional.ofNullable(this.movieMap.get(movie.getId()));
    }

    @Override
    public boolean removeMovieById(Long id) {
        if (!this.movieMap.containsKey(id)) {
            return false;
        }
        this.movieMap.remove(id);
        return true;
    }
}
