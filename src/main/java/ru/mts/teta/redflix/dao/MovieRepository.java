package ru.mts.teta.redflix.dao;

import ru.mts.teta.redflix.model.Movie;

import java.util.List;
import java.util.Optional;


public interface MovieRepository {
    List<Movie> findAll();
    Optional<Movie> addMovie(Movie movie);
    Optional<Movie> findMovieById(Long id);
    Optional<Movie> updateMovieById(Movie movie);
    boolean removeMovieById(Long id);
}
