package ru.mts.teta.redflix.advisor.exception;

import ru.mts.teta.redflix.model.Movie;

public class NoSuchMovieException extends RuntimeException {
	private Long id;
	private String message = "Нет фильма с заданным ID";
	
	public NoSuchMovieException(Long id) {
		this.id = id;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}