package ru.mts.teta.redflix.advisor;

import ru.mts.teta.redflix.advisor.exception.*;

import java.io.IOException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {

    @ExceptionHandler(BadDateException.class)
    public ResponseEntity<SimpleErrorResponse> handleBadDateException(BadDateException ex, WebRequest request) {
        SimpleErrorResponse error = new SimpleErrorResponse("Неверная дата: " + ex.getDate());

        return new ResponseEntity<SimpleErrorResponse>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(WrongDataException.class)
    public ResponseEntity<SimpleErrorResponse> handleWrongDataException(WrongDataException ex, WebRequest request) {

        SimpleErrorResponse error = new SimpleErrorResponse("Неверные данные");

        return new ResponseEntity<SimpleErrorResponse>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NoSuchMovieException.class)
    public ResponseEntity<SimpleErrorResponse> handleNoSuchMovieException(NoSuchMovieException ex, WebRequest request) {

        SimpleErrorResponse error = new SimpleErrorResponse("Нет фильма с таким ID: " + ex.getId());

        return new ResponseEntity<SimpleErrorResponse>(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(IOException.class)
    public ResponseEntity<SimpleErrorResponse> handleIOException(IOException ex, WebRequest request) {
        SimpleErrorResponse error = new SimpleErrorResponse("Неверная дата");

        return new ResponseEntity<SimpleErrorResponse>(error, HttpStatus.BAD_REQUEST);
    }
}