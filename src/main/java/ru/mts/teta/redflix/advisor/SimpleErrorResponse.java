package ru.mts.teta.redflix.advisor;

public class SimpleErrorResponse {
	private String message;
	private Long code;

	public SimpleErrorResponse(String message) {
		this.message = message;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}