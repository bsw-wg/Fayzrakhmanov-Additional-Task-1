package ru.mts.teta.redflix.advisor.exception;

public class WrongDataException extends RuntimeException {
	private String message = "Неверные данные";

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}