package ru.mts.teta.redflix.advisor.exception;

import java.time.LocalDateTime;

public class BadDateException extends RuntimeException {
	private String message = "Неверная дата";
	private String date;

	public BadDateException(String date) {
		this.date = date;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDate() {
		return this.date;
	}
}