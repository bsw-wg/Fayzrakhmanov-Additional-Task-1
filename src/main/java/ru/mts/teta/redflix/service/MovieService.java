package ru.mts.teta.redflix.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mts.teta.redflix.dao.MovieRepository;
import ru.mts.teta.redflix.model.Movie;

import java.util.List;
import java.util.Optional;

@Service
public class MovieService {

    @Autowired
    private MovieRepository repository;

    public List<Movie> findAll() {
        return repository.findAll();
    }

    public Optional<Movie> addMovie(Movie movie) {
        return repository.addMovie(movie);
    }

    public Optional<Movie> findMovieById(Long id) {
        return repository.findMovieById(id);
    }

    public Optional<Movie> updateMovieById(Movie movie) {
        return repository.updateMovieById(movie);
    }

    public boolean removeMovieById(Long id) {
        boolean result = repository.removeMovieById(id);
        return true;
    }
}
